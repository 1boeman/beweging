(function() {
    var canvas = document.querySelector('#canvas'); 
    var ctx = canvas.getContext("2d");
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    var sprite = false; 
    var dirConfig = [];
    var currentDir = 0; 


    class Sprite {
        config = {};
        speed = 1;
        direction = '';
        images = [];
        frames = []; 
        frameBased = false; 
        rootDir = '';
        loadedImages = {}; 
        allImagesLoaded = false; 
        millisPerImage = 200;
        lastImageUpdate = 0; 
        currentImageIndex = 0;
        x=0;
        y=0;
        width=0;
        height=0; 
        scale = 1;      
        scaledWidth=0;
        scaledHeight=0;

 
        constructor(config,rootDir){
            this.config = config; 
            this.rootDir = rootDir; 
            this.speed = config.speed;
            if ("frames" in config){
                this.frames = config.frames;
                this.frameBased = true; 
            }else{
                this.images = config.images;
            }
            this.direction = config.direction;
            if (config.hasOwnProperty('millisPerImage')){
                this.millisPerImage = config.millisPerImage; 
            }
            this.loadImages(rootDir);
        }



        loadImages(dir){
            var that = this; 
            if (this.frameBased){
                for (let f of this.frames){
                    this.images.push(f["img"]);
                }  
            }

            if (this.images.length) {
                for (let i=0; i < this.images.length; i++){
                    let j = new Image(); 
                    j.onload = function(){
                        that.loadedImages[that.images[i]] = j; 
                        that.checkImagesLoaded();
                    }
                    j.src = dir+'/'+this.images[i];
                }
            } else {
                alert('no images!')
            }
        }



        setDimensions(img){
            let scaleH=1,scaleV=1;
            this.width = img.width;
            this.height = img.height;               
            if (img.width > canvas.width){
                scaleH = canvas.width/img.width;
            }               
            if (img.height > canvas.height){
                scaleV = canvas.height/img.height;
            }
            if (scaleV||scaleH){
              this.scale = Math.min(scaleV,scaleH);
            }
            this.scaledWidth = this.scale*this.width;
            this.scaledHeight = this.scale*this.height;

            // initial positioning

            if (this.direction == 'east'){
                this.y  = (canvas.height/2) - (this.scaledHeight/2)
                this.x = -1* this.scaledWidth;
            } else if (this.direction == 'west') {
                this.y  = (canvas.height/2) - (this.scaledHeight/2)
                this.x = canvas.width;
            } else if (this.direction == 'north'){
                this.y = canvas.height;
                this.x = canvas.width/2 - this.scaledWidth/2;
            } else if (this.direction == 'south'){
                this.y = -1* this.scaledHeight;
                this.x = canvas.width/2 - this.scaledWidth/2;
            }

        }

        checkImagesLoaded(){
            let loaded = false; 
            if (this.allImagesLoaded || 
                    (this.images.length && 
                        (Object.keys(this.loadedImages).length == this.images.length))) {
                loaded =  true;
            } else { // extra check used if images are re-used in the this.images array;
                let checked=0; 
                for (let i of this.images){
                    if (this.loadedImages.hasOwnProperty(i)){
                        checked++;
                    } 
                    if (checked == this.images.length) loaded = true; 
                }
            }

            if (loaded){
                this.allImagesLoaded = true; 
                this.setDimensions( this.loadedImages[this.images[0]])
                return true; 
            }

            return false;
        }


        update(){
            if (this.frameBased){
                let currentFrame = this.frames[this.currentImageIndex];
                if ("speed" in currentFrame){
                    this.speed = currentFrame["speed"];
                } else {
                    this.speed = this.config["speed"];
                }  

                if ("direction" in currentFrame){
                    this.direction = currentFrame["direction"];
                } else {
                    this.direction = this.config["direction"];
                }  

                if ("millisPerImage" in currentFrame){
                    this.millisPerImage = currentFrame["millisPerImage"];
                } else {
                    this.millisPerImage = this.config["millisPerImage"];
                }  
            }
             
            if (this.direction == 'east'){
                this.x += this.speed;
            } else if (this.direction == 'west') {
                this.x -= this.speed; 
            } else if (this.direction == 'north'){
                this.y -= this.speed;
            } else if(this.direction == 'south'){
                this.y += this.speed;
            }


            let timestamp = Date.now();
            if (timestamp-this.lastImageUpdate >= this.millisPerImage){
                this.lastImageUpdate = timestamp;
                this.currentImageIndex++;
                if (this.currentImageIndex >= this.images.length){
                    this.currentImageIndex = 0; 
                }
            }

        }


        isgone(){
            if (!this.width) return false; // not fully loaded yet
            if(this.direction == 'east' && this.x > canvas.width){
                return true;
            } else if (this.direction == 'west' && this.x < -1*this.scaledWidth){
                return true;
            } else if (this.direction == 'north' && this.y < -1*this.scaledHeight){
                return true;
            } else if (this.direction == 'south' && this.y > canvas.height){
                return true;
            }

            return false; 
        }


        draw(){
            if (this.allImagesLoaded || this.checkImagesLoaded()){
                ctx.clearRect(0, 0, canvas.width, canvas.height);
                ctx.drawImage(this.loadedImages[this.images[this.currentImageIndex]],this.x,this.y,this.scaledWidth,this.scaledHeight);
            }
            this.update()
        }
    }


    function getInstance(){
        var dir = dirConfig[currentDir];

        if(window.location.hash) {
            dir = './instances/' + window.location.hash.replace("#","");
        } 

        ajaxGet(dir + '/config.json',function(response){
            sprite  = new Sprite(JSON.parse(response.responseText),dir);
            currentDir++;
            if (currentDir >= dirConfig.length){
                currentDir = 0;
            }
        });
    }


    function getConfig(){
        if (!dirConfig.length){
            ajaxGet('./get_instance.php',function(r){
                dirConfig = JSON.parse(r.responseText); 
                getInstance();
           });
        } else {
            getInstance();
        }
    }


    function ajaxGet(url, callback){
        var r = new XMLHttpRequest();
        r.onreadystatechange = function(){
            if (r.readyState === XMLHttpRequest.DONE) {
                if (r.status === 200) {
                    callback(r)
                } else {
                    alert(':-(');
                    location.reload();
                }
            } 
        };
        r.open('GET',url, true);
        r.send();
    }



    function loop(){
        if(sprite){
            sprite.draw();
            if (sprite.isgone()){
                sprite = false;
                getConfig();
            }
        }
        window.requestAnimationFrame(loop)
    }

    // initiate
    getConfig();
    loop();

}());
